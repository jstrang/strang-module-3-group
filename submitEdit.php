<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: news.php');
    }
    require 'database.php';
    $story_id = $_POST['story_id'];
    $comment_id = $_POST['comment_id'];
    $comment = $_POST['comment'];
    
    if($_SESSION['token'] !== $_POST['token']) {
        die("Request Forgery Detected");
    }
    
    $stmt = $mysqli->prepare("UPDATE comments SET comment = ? WHERE id=?");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('si', $comment, $comment_id);
    $stmt->execute();
    $stmt->close();
    header('Location: readNews.php?id='.$story_id);
    
?>