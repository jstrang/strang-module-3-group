<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: news.php');
    }
    require 'database.php';
    if($_SESSION['token'] !== $_POST['token']) {
        die("Request Forgery Detected");
    }
    $story_id = $_POST['story_id'];
    $comment_id = $_POST['comment_id'];
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo "Edit Comment"; ?></title>
        <style type="text/css">
            body{
                width: 760px;
                background-color: teal;
                margin: 0 auto;
                padding: 100px;
                font:12px/16px Verdana, sans-serif;
                text-align: left;
            }
            div#main{
                background-color: #FFF;
                margin: 0;
                padding: 10px;
            }
            </style>
    </head>
    <body>
	<form action=newsLogout.php method="GET">
            <input type="submit" value="logout" name="logout"/>
        </form>
	<form action=newsHome.php method="GET">
	    <input type="submit" value="Home">
	</form>
        <h1> <?php echo $title;?></h1>
        <h2> Comment Edit: </h2>
        <form action=submitEdit.php method="POST">
            <textarea cols="50" rows="10" name="comment">
                </textarea>
                <input type="submit" value="submit"/>
                <input type="hidden" name="story_id" value="<?php echo $story_id; ?>" />
		<input type="hidden" name="comment_id" value="<?php echo $comment_id ?>" />
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
        </form>
    </body>
</html>