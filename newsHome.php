<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>User Registration</title>
        <style type="text/css">
            body{
                width: 760px;
                background-color: teal;
                margin: 0 auto;
                padding: 100px;
                font:12px/16px Verdana, sans-serif;
                text-align: left;
            }
            div#main{
                background-color: #FFF;
                margin: 0;
                padding: 10px;
            }
            </style>
    </head>
    <body>
        <h1> Welcome to 330 News!</h1>
        <p>
            <form action=newsLogout.php method="GET">
                <input type="submit" value="logout" name="logout"/>
            </form>
        </p>
        <p>Or click here to submit a new story: </p>
            <form action=addStory.php method="GET">
                <input type="submit" name="Submit"/>
            </form>
            <h2>
                <?php
                require 'database.php';
                $stmt = $mysqli->prepare("SELECT stories.id, title, users.username FROM stories JOIN users on (stories.author_id=users.id) ORDER BY time");
                if(!stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->execute();
                $stmt->bind_result($id, $title, $authorname);
                
                
                while($stmt->fetch()){
                    $safeid = htmlentities($id);
                    $safetitle = htmlentities($title);
                    $safeauthor = htmlentities($authorname);
                    $tempToken = $_SESSION['token'];
                    echo "<a href=\"readNews.php?id=$safeid\"> $safetitle </a><br><br>";
                    echo "by: <a href=\"userPage.php?username=$safeauthor\"> $safeauthor </a> <br><br>";
                    if ($authorname == $_SESSION['username']) {
                        echo "<form action=deleteStory.php method=\"POST\">
                            <input type=\"submit\" value = \"delete\" />
                            <input type=\"hidden\" name=\"story_id\" value=\"$id\"/>
                            <input type=\"hidden\" name=\"token\" value=\"$tempToken\" />
                            </form>";
                        echo "<form action=editStory.php method=\"POST\">
                            <input type=\"submit\" value = \"edit\" />
                            <input type=\"hidden\" name=\"story_id\" value=\"$id\"/>
                            <input type=\"hidden\" name=\"token\" value=\"$tempToken\" />
                            </form>";
                    }
                    echo "<br>";
                }
                
                
                
                ?>
            </h2>
    </body>
</html>