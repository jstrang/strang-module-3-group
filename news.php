<?php
    session_start();
    if (isset($_SESSION['username'])){
        header('Location: newsHome.php');
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>News</title>
        <style type="text/css">
            body{
                width: 760px;
                background-color: teal;
                margin: 0 auto;
                padding: 100px;
                font:12px/16px Verdana, sans-serif;
                text-align: left;
            }
            div#main{
                background-color: #FFF;
                margin: 0;
                padding: 10px;
            }
            </style>
    </head>
    <body>
        <h1>News</h1><br>
        <h3>Login:</h3><br>
        <form action=loginAttempt.php method="POST">
            UserName:<input type="text" name="UserName"/><br>
            Password:<input type="password" name="pwd" /><br>
            <input type="submit" value="Login"/>
        </form> <br>
        <h3>Or, click here to register:</h3><br>
        <form action=newsRegistration.html method="GET">
        	<input type="submit" value="Register"/>
        </form>
        <h3>Or, proceed to stories without logging in:</h3>
        <form action=newsHome.php method="GET">
            <input type="submit" value="View"/>
        </form>
    </body>
</html>