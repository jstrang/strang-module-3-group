<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: news.php');
    }
    require 'database.php';
    $story_id = $_POST['story_id'];
    $story = $_POST['story'];
    
    if($_SESSION['token'] !== $_POST['token']) {
        die("Request Forgery Detected");
    }
    
    $stmt = $mysqli->prepare("UPDATE stories SET story = ? WHERE id=?");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('si', $story, $story_id);
    $stmt->execute();
    $stmt->close();
    header('Location: newsHome.php');
    
?>