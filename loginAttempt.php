<?php
require 'database.php';

$username = $_POST['UserName'];
$pwd = $_POST['pwd'];

$stmt = $mysqli->prepare("SELECT COUNT(*), pwd_hash, id FROM users WHERE username=?");
if(!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}

$stmt->bind_param('s', $username);
$stmt->execute();
$stmt->bind_result($cnt, $pwd_hash, $id);
$stmt->fetch();
$stmt->close();

if( $cnt == 1 && crypt($pwd, $pwd_hash)==$pwd_hash){
	// Login succeeded!
        session_start();
	$_SESSION['username'] = $username;
        $_SESSION['user_id'] = $id;
        $_SESSION['token'] = substr(md5(rand()),0,10);
	// Redirect to your target page
        header('Location: newsHome.php');
}else{
        header('Location: news.php');
}