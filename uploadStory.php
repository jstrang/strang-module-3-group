<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: news.php');
    }
    require 'database.php';
    if($_SESSION['token'] !== $_POST['token']) {
        die("Request Forgery Detected");
    }
    
    $story = $_POST['story'];
    $title = $_POST['title'];
    $username = $_SESSION['username'];
    $id = $_SESSION['user_id'];
    
    $stmt = $mysqli->prepare("INSERT INTO stories (author_id, title, story) VALUES (?, ?, ?)");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('iss', $id, $title, $story);
    $stmt->execute();
    $stmt->close();
    header('Location: newsHome.php');
    
?>