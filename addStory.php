<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: unauthorized.html');
        exit;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>User Registration</title>
        <style type="text/css">
            body{
                width: 760px;
                background-color: teal;
                margin: 0 auto;
                padding: 100px;
                font:12px/16px Verdana, sans-serif;
                text-align: left;
            }
            div#main{
                background-color: #FFF;
                margin: 0;
                padding: 10px;
            }
            </style>
    </head>
    <body>
        <form action=newsLogout.php method="GET">
            <input type="submit" value="logout" name="logout"/>
        </form>
	<form action=newsHome.php method="GET">
	    <input type="submit" value="Home">
	</form>
        <h2>Submit Story: </h2>
        <p>
            <form action=uploadStory.php method="POST">
                <input type="text" name="title"/><br>
                <textarea cols="50" rows="10" name="story">
                </textarea>
                <input type="submit" value="submit"/>
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            </form>
        </p>
    </body>
</html>