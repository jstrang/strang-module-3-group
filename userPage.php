<?php
    session_start();
    require 'database.php';
    $user = $_GET['username'];
    $stmt = $mysqli->prepare("SELECT email_address, first_name, last_name FROM users WHERE username=?");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->bind_result($email, $first, $last);
    $stmt->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $user; ?></title>
        <style type="text/css">
            body{
                width: 760px;
                background-color: teal;
                margin: 0 auto;
                padding: 100px;
                font:12px/16px Verdana, sans-serif;
                text-align: left;
            }
            div#main{
                background-color: #FFF;
                margin: 0;
                padding: 10px;
            }
            </style>
    </head>
    <body>
        <h1><?php echo $user; ?></h1><br><br>
        <h2>First Name: <?php echo htmlentities($first); ?></h2>
        <h2>Last Name: <?php echo htmlentities($last); ?></h2>
        <h2>Email: <?php echo htmlentities($email); ?></h2><br><br>
        <h2>Published Stories: </h2>
        <h3>
            <?php
               $stmt = $mysqli->prepare("SELECT stories.id, title FROM stories JOIN users on (stories.author_id=users.id) WHERE users.username=? ORDER BY time");
                if(!stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->bind_param('s',$user);
                $stmt->execute();
                $stmt->bind_result($id, $title);
                
                
                while($stmt->fetch()){
                    $safeid = htmlentities($id);
                    $safetitle = htmlentities($title);
                    echo "<a href=\"readNews.php?id=$safeid\"> $safetitle </a><br><br>";
                }
                $stmt->close();
            ?>
        </h3>
    </body>
</html>
