<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: unauthorized.html');
    }
    require 'database.php';
    
    if($_SESSION['token'] !== $_POST['token']) {
        die("Request Forgery Detected");
    }
    
    $story_id = $_POST['story_id'];
    $comment = $_POST['comment'];
    $author_id = $_SESSION['user_id'];
    
    $stmt = $mysqli->prepare("INSERT INTO comments (author_id, story_id, comment) VALUES (?, ?, ?)");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('iis', $author_id, $story_id, $comment);
    $stmt->execute();
    $stmt->close();
    header('Location: readNews.php?id='.$story_id);
    
?>