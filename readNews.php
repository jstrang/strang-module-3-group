<?php
    session_start();
    require 'database.php';
    
    $story_id = $_GET['id'];
    
    $stmt = $mysqli->prepare("SELECT story, title FROM stories WHERE id=?");
    
    if(!stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    
    $stmt->bind_param('i', $story_id);
    $stmt->execute();
    $stmt->bind_result($story, $title);
    $stmt->fetch();
    $stmt->close();
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <style type="text/css">
            body{
                width: 760px;
                background-color: teal;
                margin: 0 auto;
                padding: 100px;
                font:12px/16px Verdana, sans-serif;
                text-align: left;
            }
            div#main{
                background-color: #FFF;
                margin: 0;
                padding: 10px;
            }
            </style>
    </head>
    <body>
        <form action=newsLogout.php method="GET">
            <input type="submit" value="logout" name="logout"/>
        </form>
	<form action=newsHome.php method="GET">
	    <input type="submit" value="Home">
	</form>
        <h1> <?php echo $title;?></h1>
        <p>
            <?php echo htmlentities($story); ?>
        </p> <br><br><br>
        <h2> Comments: </h2>
        <form action=addComment.php method="POST">
            <textarea cols="50" rows="10" name="comment">
                </textarea>
                <input type="submit" value="submit"/>
                <input type="hidden" name="story_id" value="<?php echo $story_id; ?>" />
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
        </form>
            <?php
                require 'database.php';
                $stmt = $mysqli->prepare("SELECT comment, comments.id, users.username FROM comments JOIN users on (comments.author_id=users.id) WHERE story_id=? ORDER BY time");
                if(!stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->bind_param('i', $story_id);
                $stmt->execute();
                $stmt->bind_result($comment, $comment_id, $username);
                
                while($stmt->fetch()){
                    $safecomment = htmlentities($comment);
                    $safename = htmlentities($username);
                    $tempToken = $_SESSION['token'];
                    echo "<h3> <a href=\"userPage.php?username=$safename\"> $safename </a>: </h3><p>$safecomment</p>";
                    if ($safename == $_SESSION['username']) {
                        echo "<form action=deleteComment.php method=\"POST\">
                            <input type=\"submit\" value = \"delete\" />
                            <input type=\"hidden\" name=\"comment_id\" value=\"$comment_id\"/>
                            <input type=\"hidden\" name=\"story_id\" value=\"$story_id\"/>
                            <input type=\"hidden\" name=\"token\" value=\"$tempToken\" />
                            </form>";
                        echo "<form action=editComment.php method=\"POST\">
                            <input type=\"submit\" value = \"edit\" />
                            <input type=\"hidden\" name=\"comment_id\" value=\"$comment_id\"/>
                            <input type=\"hidden\" name=\"story_id\" value=\"$story_id\"/>
                            <input type=\"hidden\" name=\"token\" value=\"$tempToken\" />
                            </form>";
                    }
                    echo "<br>";
                }
                $stmt->close();
                ?>
    </body>
</html>