<?php
    session_start();
    if (!isset($_SESSION['username'])){
        session_destroy();
        header('Location: news.php');
    }
    require 'database.php';
    if($_SESSION['token'] !== $_POST['token']) {
        die("Request Forgery Detected");
    }
    $story_id = $_POST['story_id'];
    
    $stmt = $mysqli->prepare("DELETE FROM stories WHERE id=?");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('i', $story_id);
    $stmt->execute();
    $stmt->close();
    header('Location: newsHome.php');
?>